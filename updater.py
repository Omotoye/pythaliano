#!/usr/bin/python
import sys


class WordListUpdate:
    def __init__(self):
        self.words_file = "project_files/words.txt"
        self.word_list_content = self._get_word_list_content()
        self.existing_word = []
        self.state = 1
        self.run()

    def _check_word_existence(self, new_word):
        result = False
        for word in self.word_list_content:
            if new_word in word:
                self.existing_word.append(word)
                result = True
            else:
                continue
        return result

    def _get_word_input(self):
        self.new_english_word = input("Enter the English Meaning: ")
        self.new_italian_word = input("Enter the Italian Word: ")

    def _get_word_list_content(self):
        with open(self.words_file, "r") as f:
            content = f.readlines()
        for index, line in enumerate(content):
            content[index] = line.rstrip().split(" ")
        return content

    def run(self):
        while True:
            if self.state == 1:
                print("\nEnter an Italian word and provide it's English Meaning: ")
                self._get_word_input()
                if self._check_word_existence(
                    self.new_italian_word
                ) or self._check_word_existence(self.new_english_word):
                    print(
                        f"The word {self.new_english_word} : {self.new_italian_word} could not be added."
                    )
                    print(f"The word already exist as: {self.existing_word}")
                    self.existing_word = []
                else:
                    self.update()
                    print("The new word has been added")

            elif self.state == 2:
                self.save_content()
                break
            while True:
                try:
                    self.state = int(
                        input("\n\nEnter 1 to add new word, enter 2 to save and exit: ")
                    )
                    break
                except ValueError as e:
                    print(f"Exception: {e!r}", file=sys.stderr)

    def update(self):
        new_line = [
            str(len(self.word_list_content) + 1),
            self.new_english_word,
            self.new_italian_word,
            "1",
        ]
        self.word_list_content.append(new_line)

    def save_content(self):
        with open(self.words_file, "w") as f:
            for index, line in enumerate(self.word_list_content):
                line = " ".join(line)
                if (index + 1) == len(self.word_list_content):
                    f.write(line)
                else:
                    f.write(line + "\n")


if __name__ == "__main__":
    try:
        WordListUpdate()
    except KeyboardInterrupt:
        pass
