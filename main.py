#!/usr/bin/python

import linecache
import fileinput
import re
import sys
import random

from numpy import record

PROMPT = """\n\nWelcome to Pythaliano a fun way to learn Italian

1. Learn up to 2000 Words 
2. Learn up to 550 Phrases
3. Learn words from Music

Enter the number cooresponding to the activity you want to do: """


class Pythaliano:
    def __init__(self):
        self._prompt = PROMPT
        self.words_file = "project_files/words.txt"
        self.records_file = "project_files/records.txt"
        self.rep = {"0": [1, 1], "1": [3, 5], "2": [3, 4], "3": [2, 3], "4": [1, 2]}
        # self.total_line_num = self._get_total_line_num()
        self.words_level = self._get_words_level()
        self.words_per_day = 50
        self.ita_index = list(range(1, self.words_level + 1))
        self.eng_index = list(range(1, self.words_level + 1))
        self.one_lang_completed = False
        if self._is_finished():
            self._init_records()
        self._init_words()
        while self._accept_request() == False:
            self._accept_request()

    def _accept_request(self):
        try:
            self._request = int(input(self._prompt))
            if self._request == 1:  # Learn up to 2000 words
                self.practice_words()
                return True
            elif self._request == 2:  # Learn up to 550 Phrases
                print("Coming Soon!!!")
                return True
            elif self._request == 3:  # Learn words from Music
                print("Coming Soon!!!")
                return True
            else:
                print("\nInvalid request input\n")
                return False
        except ValueError:
            print("\nPlease Enter an Integer Value!\n")
            return False

    def _get_total_line_num(self):
        return sum(1 for line in open(self.words_file))

    # def _generate_random_word(self):
    #     completed_words = []
    #     while True:
    #         while True:
    #             random_index = random.randint(1, self.words_level)
    #             self.random_lang = random.randint(1, 2)
    #             if (str(random_index) + str(self.random_lang)) not in completed_words:
    #                 break
    #         particular_line = linecache.getline(self.words_file, random_index)
    #         linecache.clearcache()
    #         self.content = particular_line.rstrip().split(" ")
    #         record_line = linecache.getline(self.records_file, int(self.content[0]))
    #         linecache.clearcache()
    #         record_content = record_line.rstrip().split(" ")
    #         if int(record_content[self.random_lang]) < self.rep:
    #             # print(f"content: {self.content}")
    #             result = True
    #             break
    #         completed_words.append(str(random_index) + str(self.random_lang))
    #         if self.check_reps_left() == False:
    #             result = False
    #             break
    #     return result

    def generate_word(self):
        while True:
            if self.one_lang_completed == False:
                self.random_lang = random.randint(1, 2)
            else:
                if len(self.eng_index) != 0:
                    self.random_lang = 1
                elif len(self.ita_index) != 0:
                    self.random_lang = 2
            if self.random_lang == 1:  # english
                if len(self.eng_index) == 1:
                    self.random_index = self.eng_index[0]
                else:
                    self.random_index = self.eng_index[
                        random.randint(0, (len(self.eng_index) - 1))
                    ]
            else:  # italian
                if len(self.ita_index) == 1:
                    self.random_index = self.ita_index[0]
                else:
                    self.random_index = self.ita_index[
                        random.randint(0, (len(self.ita_index) - 1))
                    ]

            self.content = self.get_line(self.words_file, self.random_index)
            self.record_content = self.get_line(self.records_file, self.random_index)
            if self.is_practised():
                if self.is_tested():
                    if self.random_lang == 1:
                        self.eng_index.remove(self.random_index)
                    else:
                        self.ita_index.remove(self.random_index)
                else:
                    state = "test"
                    break
            else:
                state = "practise"
                break

            if self.is_reps_completed():
                state = "completed"
                break
        # print(f"state: {state}")
        return state

    def get_line(self, file_path, line_index):
        particular_line = linecache.getline(file_path, line_index)
        linecache.clearcache()
        return particular_line.rstrip().split(" ")

    def is_practised(self):
        required_reps = str(self.rep[self.content[3]][0])
        if self.record_content[self.random_lang] == required_reps:
            return True
        else:
            return False

    def is_tested(self):
        required_reps = str(self.rep[self.content[3]][1])
        if self.record_content[self.random_lang + 2] == required_reps:
            return True
        else:
            return False

    def is_reps_completed(self):
        if len(self.eng_index) == 0 and len(self.ita_index) == 0:
            return True
        elif len(self.eng_index) == 0 or len(self.ita_index) == 0:
            self.one_lang_completed = True
            return False
        else:
            return False

    def _init_records(self):
        with open(self.records_file, "w") as f:
            for i in range(1, self.words_level + 1):
                if i == self.words_level:
                    f.write(str(i) + " 0 0 0 0")
                else:
                    f.write(str(i) + " 0 0 0 0\n")

    def _init_words(self):
        particular_line = linecache.getline(self.words_file, self.words_level)
        linecache.clearcache()
        content = particular_line.rstrip().split(" ")

        if content[3] == 2:
            self.words_level = self.words_level + self.words_per_day

    def _get_words_level(self):
        with open("project_files/level_state.txt", "r") as f:
            return int(f.readline().rstrip())

    def _is_finished(self):
        linecache.clearcache()
        result = bool(
            int(linecache.getline("project_files/level_state.txt", 2).rstrip())
        )
        return result

    def practice_words(self):
        while 1:
            state = self.generate_word()
            if state == "completed":
                print("\n\nYou have completed the Reps!!!\n\n")
                break
            if self.random_lang == 1:
                if state == "practise":
                    print(f"[{self.content[1]} -> {self.content[2]}] : ", end="")
                answer = input(f"{self.content[self.random_lang]} -> ")
                if answer.lower() == self.content[2].lower():
                    print("Passed\n\n")
                    self._update_records("passed", state)
                else:
                    print("Failed\n\n")
                    self._update_records("failed", state)
            elif self.random_lang == 2:
                if state == "practise":
                    print(f"[{self.content[2]} -> {self.content[1]}] : ", end="")
                answer = input(f"{self.content[self.random_lang]} -> ")
                if answer.lower() == self.content[1].lower():
                    print("Passed\n\n")
                    self._update_records("passed", state)
                else:
                    print("Failed\n\n")
                    self._update_records("failed", state)

    def _update_records(self, result, state):
        record_line = " ".join(self.record_content)
        if state == "practise":
            if result == "passed":
                self.record_content[self.random_lang] = str(
                    int(self.record_content[self.random_lang]) + 1
                )
                self.record_content = " ".join(self.record_content)
                self.replacement(self.records_file, record_line, self.record_content)
            elif result == "failed":
                self.record_content[self.random_lang] = "0"
                self.record_content = " ".join(self.record_content)
                self.replacement(self.records_file, record_line, self.record_content)
        elif state == "test":
            if result == "passed":
                self.record_content[self.random_lang + 2] = str(
                    int(self.record_content[self.random_lang + 2]) + 1
                )
                self.record_content = " ".join(self.record_content)
                self.replacement(self.records_file, record_line, self.record_content)
            elif result == "failed":
                self.record_content[self.random_lang] = "0"
                self.record_content[self.random_lang + 2] = "0"
                self.record_content = " ".join(self.record_content)
                self.replacement(self.records_file, record_line, self.record_content)

    def replacement(self, file, previousw, nextw):
        for line in fileinput.input(file, inplace=1):
            line = line.replace(previousw, nextw)
            sys.stdout.write(line)

    def check_reps_left(self):
        result = False
        for i in range(1, self.words_level + 1):
            record_line = linecache.getline(self.records_file, i)
            linecache.clearcache()
            record_content = record_line.rstrip().split(" ")
            if int(record_content[1]) < self.rep or int(record_content[2]) < self.rep:
                result = True
                break
        return result


if __name__ == "__main__":
    try:
        Pythaliano()
    except KeyboardInterrupt:
        pass
